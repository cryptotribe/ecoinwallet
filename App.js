/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useRef } from 'react';
import { Image, AppState } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Provider } from 'react-redux';

import createStore from './app/redux/store';

const store = createStore();

const AccountsStack = createStackNavigator();
const MainTab = createBottomTabNavigator();
const MainStack = createStackNavigator();
const TransferStack = createStackNavigator();

import {
  AccountsScreen,
  TokenDetailsScreen,
  BackupAllKeysScreen,
  PrivateKeyBackupScreen,
  RegisterAddressEcoinScreen,
  RegisterAddressEmailScreen,
  ConnectAccountScreen,
  TransferScreen,
  PinCodeScreen,
} from './app/screens';

const AccountsStackScreen = () => {
  return (
    <AccountsStack.Navigator headerMode={'none'}>
      <AccountsStack.Screen
        name="Accounts"
        component={AccountsScreen}
      />
      <AccountsStack.Screen
        name="ConnectAccount"
        component={ConnectAccountScreen}
      />
      <AccountsStack.Screen
        name="TokenDetails"
        component={TokenDetailsScreen}
      />
      <AccountsStack.Screen
        name="BackupAllKeys"
        component={BackupAllKeysScreen}
      />
      <AccountsStack.Screen
        name="PrivateKeyBackup"
        component={PrivateKeyBackupScreen}
      />
      <AccountsStack.Screen
        name="RegisterAddressEmail"
        component={RegisterAddressEmailScreen}
      />
      <AccountsStack.Screen
        name="RegisterAddressEcoin"
        component={RegisterAddressEcoinScreen}
      />
    </AccountsStack.Navigator>
  );
};

const TransferStackScreen = () => {
  return (
    <TransferStack.Navigator headerMode={'none'}>
      <TransferStack.Screen
        name="Transfer"
        component={TransferScreen}
      />
    </TransferStack.Navigator>
  );
};


const tabScreenOptions = ({ route }) => ({
  tabBarIcon: ({ focused, color, size }) => {
    let icon;
    if (route.name === 'Accounts') {
      icon = require('./assets/icons/accounts.png');
    } else if (route.name === 'Transfer') {
      icon = require('./assets/icons/transfer.png');
    }
    return <Image source={icon} style={{ tintColor: color }} />;
  },
});

const MainTabScreen = () => {
  return (
    <MainTab.Navigator
      screenOptions={tabScreenOptions}
      tabBarOptions={{
        showLabel: false,
      }}>
      <MainTab.Screen name={'Accounts'} component={AccountsStackScreen} />
      <MainTab.Screen name={'Transfer'} component={TransferStackScreen} />
    </MainTab.Navigator>
  );
};

const App = () => {
  const navigationRef = useRef(null);

  return (
    <Provider store={store}>
      <NavigationContainer ref={navigationRef}>
        <MainStack.Navigator headerMode={'none'}>
          <MainStack.Screen name="MainTab" component={MainTabScreen} />
        </MainStack.Navigator>
      </NavigationContainer>
    </Provider>
  );
};

export default App;
