import React, { useState } from 'react';
import { Fio, Ecc } from '@fioprotocol/fiojs';
import { SafeAreaView, View, Image, TouchableOpacity, Alert } from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import styles from './TransferScreen.style';
import {
  KHeader,
  KButton,
  KInput,
  KSelect,
  KText,
} from '../../components';
import { connectAccounts } from '../../redux';
import { getAccount } from '../../eos/eos';
import { getChain, getEndpoint } from '../../eos/chains';
import { getTokens, getBalance, transferToken } from '../../eos/tokens';
import { PRIMARY_BLUE } from '../../theme/colors';
import { log } from '../../logger/logger';


const TransferScreen = props => {
  const [isLoading, setIsLoading] = useState(false);
  const [tokenBalance, setTokenBalance] = useState(0);
  const [toAccountName, setToAccountName] = useState('');
  const [amount, setAmount] = useState('');
  const [memo, setMemo] = useState('');
  const [token, setToken] = useState();
  const [toActor, setToActor] = useState('');
  const [toPubkey, setToPubkey] = useState('');
  const [addressInvalidMessage, setAddressInvalidMessage] = useState();

  const {
    navigation: { navigate, goBack },
    accountsState: { accounts, activeAccountIndex, addresses },
  } = props;

  const chainCode = 'TLOS';
  const fioEndpoint = getEndpoint('FIO');

  const tokens = getTokens();
  if(!token) {
    setToken(tokens[0]);
  }

  var fioAccount;
  var telosAccount;

  accounts.map((value, index, array) => {
    var account = value;
    if (account.chainName === 'TLOS' || account.chainName === 'Telos' ) {
      telosAccount = account;
    }
    if (account.chainName === 'FIO') {
      fioAccount = account;
    }
  });

  const handleTokenBalance = (jsonArray) => {
    try {
      if(jsonArray && jsonArray.length > 0) {
        setTokenBalance(jsonArray[0]);
      } else if(telosAccount && token) {
        setTokenBalance('0 '+token.name);
      }
    } catch(err) {
      console.log(err);
    }
  }

  const _validateAddress = address => {
    if (address.length >= 3 && address.indexOf('@') > 0 && address.indexOf('@') < address.length-1) {
      fetch(fioEndpoint + '/v1/chain/avail_check', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          fio_name: address,
        }),
      })
        .then(response => response.json())
        .then(json => updateAvailableState(json.is_registered, address))
        .catch(error => updateAvailableState(-1, address, error));
    }
  };

  const updateAvailableState = (regcount, address, error) => {
    if (regcount === 0) {
      setAddressInvalidMessage('Invalid FIO address!');
      setToPubkey('');
    } else if (regcount === 1) {
      setAddressInvalidMessage('');
      loadToPubkey(address);
    } else if (error) {
      setToPubkey('');
      setAddressInvalidMessage('Error validating FIO address');
      log({
        description: '_validateAddress - fetch ' + fioEndpoint + '/v1/chain/avail_check',
        cause: error,
        location: 'TokenDetailsScreen'
      });
    }
  };

  const loadToPubkey = async address => {
    fetch(fioEndpoint + '/v1/chain/get_pub_address', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        "fio_address": address,
        "chain_code": chainCode,
        "token_code": chainCode,
      }),
    })
      .then(response => response.json())
      .then(json => processToPubkeyUpdate(json.public_address))
      .catch(error => log({
        description: 'loadToPubkey - fetch [' + chainCode + '] ' + fioEndpoint + '/v1/chain/get_pub_address ['+address+']',
        cause: error,
        location: 'TokenDetailsScreen'
      })
    );
  };

  const processToPubkeyUpdate = async (toAccountPubkey) => {
    const chain = getChain(chainCode);
    if(chain) { // EOSIO chain
      const [toActorValue, toPubkeyValue] = toAccountPubkey.split(',');
      const toAccountInfo = await getAccount(toActorValue, chain);
      if (!toAccountInfo) {
        Alert.alert('Error fetching account data for '+toActor+' on chain '+chainCode);
        return;
      }
      setToActor(toActorValue);
      setToPubkey(toPubkeyValue);
    } else { // Non EOSIO chain - no 'actor,pubkey' split
      setToActor('');
      setToPubkey(toAccountPubkey);
    }
  }

  const _handleToAccountChange = value => {
    setToAccountName(value);
    // If FIO addreess
    if(value.indexOf('@') > 0) {
      _validateAddress(value);
    }
  };

  const getSubtitle = () => {
    if(telosAccount && telosAccount.accountName && token) {
      return token.name + ' on ' + telosAccount.chainName + ': ' + telosAccount.accountName;
    } else if(telosAccount && telosAccount.accountName) {
      return telosAccount.chainName + ': ' + telosAccount.accountName;
    } else {
      return "No Telos account found";
    }
  };


  const _handleTransfer = async () => {
    let toAccount = toAccountName;
    if(toAccount.indexOf('@') > 0 && toActor) {
      toAccount = toActor;
    }
    try {
      setIsLoading(true);
      const res = await transferToken(toAccount, parseFloat(amount), memo, telosAccount, token);
      if (res && res.transaction_id) {
        Alert.alert('Token transfer sent in tx '+res.transaction_id);
        goBack();
      } else {
        Alert.alert("Error: " + res.message);
      }
    } catch(err) {
      Alert.alert("Transfer error: "+err);
    } finally {
      setIsLoading(false);
    }
  };

  const refreshTokenBalance = () => {
    try {
      if(telosAccount && token) {
        getBalance(telosAccount.accountName, token, handleTokenBalance);
      }
    } catch(err) {
      Alert.alert("Error loading balance for "+telosAccount.accountName);
    }
  };

  if(telosAccount) {
    refreshTokenBalance();
  }

  const handleTokenChange = (token) => {
    setToken(token);
    refreshTokenBalance();
  };

  if (fioAccount && telosAccount) {
    return (
    <SafeAreaView style={styles.container}>
      <View style={styles.inner}>
        <TouchableOpacity style={styles.backButton} onPress={goBack}>
          <MaterialIcon
            name={'keyboard-backspace'}
            size={24}
            color={PRIMARY_BLUE}
          />
        </TouchableOpacity>
        <KHeader title={'Transfer tokens'} subTitle={getSubtitle()} style={styles.header} />
        <KText>Balance: {tokenBalance}</KText>
        <KSelect
            label={'Choose token'}
            items={tokens.map(item => ({
              label: `${item.name}`,
              value: item,
            }))}
            onValueChange={handleTokenChange}
            containerStyle={styles.inputContainer}
          />
        <KInput
          label={'Sending to'}
          placeholder={'Enter receiver name'}
          value={toAccountName}
          onChangeText={_handleToAccountChange}
          containerStyle={styles.inputContainer}
          autoCapitalize={'none'}
        />
        <KText style={styles.errorMessage}>{addressInvalidMessage}</KText>
        <KInput
          label={'Amount'}
          placeholder={'Enter amount to send'}
          value={amount}
          onChangeText={setAmount}
          containerStyle={styles.inputContainer}
          autoCapitalize={'none'}
          keyboardType={'numeric'}
        />
        <KInput
          label={'Memo'}
          placeholder={'Optional memo'}
          value={memo}
          onChangeText={setMemo}
          containerStyle={styles.inputContainer}
          autoCapitalize={'none'}
        />
        <KButton
          title={'Submit transfer'}
          theme={'blue'}
          style={styles.button}
          onPress={_handleTransfer}
          isLoading={isLoading}
          renderIcon={() => (
            <Image
              source={require('../../../assets/icons/transfer.png')}
              style={styles.buttonIcon}
            />
          )}
        />
      </View>
    </SafeAreaView>
    );
  } else {
    return (
    <SafeAreaView style={styles.container}>
      <View style={styles.inner}>
        <TouchableOpacity style={styles.backButton} onPress={goBack}>
          <MaterialIcon
            name={'keyboard-backspace'}
            size={24}
            color={PRIMARY_BLUE}
          />
        </TouchableOpacity>
        <KHeader title={'Transfer not available'} subTitle={'Please create or import accounts'} style={styles.header} />
      </View>
    </SafeAreaView>
    );
  }

};

export default connectAccounts()(TransferScreen);
