import React, { useState } from 'react';
import {
  View,
  SafeAreaView,
  Image,
  Alert,
  TouchableOpacity,
} from 'react-native';
import { Fio, Ecc } from '@fioprotocol/fiojs';
import ecc from 'eosjs-ecc-rn';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import styles from './ConnectAccountScreen.style';
import { KHeader, KButton, KInput } from '../../components';
import { getChain } from '../../eos/chains';
import { connectAccounts } from '../../redux';
import { PRIMARY_BLUE } from '../../theme/colors';
import { getAccount } from '../../eos/eos';
import { log } from '../../logger/logger';
import algosdk from 'algosdk';


const ConnectAccountScreen = props => {
  const {
    connectAccount,
    navigation: { goBack },
  } = props;

  const [telosAccountName, setTelosAccountName] = useState('');
  const [telosPrivateKey, setTelosPrivateKey] = useState('');
  const [fioPrivateKey, setFioPrivateKey] = useState('');


  const connectFioAccount = fioAddresses => {
    if (fioAddresses) {
      fioAddresses.map(function(item) {
        let address = item.fio_address;
        connectAccount({ address: address, privateKey: fioPrivateKey, chainName: 'FIO' });
      });
    }
  }

  const _handleConnect = async () => {
    // Validate inputs:
    if (!telosAccountName) {
      Alert.alert('Please enter Telos account name');
      return;
    }
    if (!telosPrivateKey) {
      Alert.alert('Please enter Telos private key');
      return;
    }
    if (!fioPrivateKey) {
      Alert.alert('Please enter FIO private key');
      return;
    }
    if (!ecc.isValidPrivate(telosPrivateKey)) {
      Alert.alert('Please input valid Telos private key');
      return;
    }
    if (!ecc.isValidPrivate(fioPrivateKey)) {
      Alert.alert('Please input valid FIO private key');
      return;
    }
    const chain = getChain('Telos');
    try {
        await getAccount(telosAccountName, chain);
    } catch (e) {
        Alert.alert('Please input valid Telos account name');
        return;
    }
    // Connect Telos account first:
    connectAccount({ accountName: telosAccountName, privateKey: telosPrivateKey, chainName: chain.name });
    // Then connect FIO account by key:
    const fioPublicKey = Ecc.privateToPublic(fioPrivateKey);
    fetch('http://fio.greymass.com/v1/chain/get_fio_names', {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            fio_public_key: fioPublicKey,
          }),
        })
        .then(response => response.json())
        .then(json => connectFioAccount(json.fio_addresses))
        .catch(error => log({
          description: '_handleConnect - fetch http://fio.greymass.com/v1/chain/get_fio_names',
          cause: error,
          location: 'ConnectAccountScreen'
        })
    );
    goBack();
  };

  return (
    <SafeAreaView style={styles.container}>
      <KeyboardAwareScrollView
        contentContainerStyle={styles.scrollContentContainer}
        enableOnAndroid>
        <View style={styles.content}>
          <KHeader
            title={'Connect accounts'}
            subTitle={'Import your FIO/Telos accounts'}
            style={styles.header}
          />
          <KInput
            label={'Telos account name'}
            placeholder={'Enter your Telos account name'}
            value={telosAccountName}
            onChangeText={setTelosAccountName}
            containerStyle={styles.inputContainer}
            autoCapitalize={'none'}
          />
          <KInput
            label={'Telos private key'}
            placeholder={'Enter your Telos private key'}
            secureTextEntry
            value={telosPrivateKey}
            onChangeText={setTelosPrivateKey}
            onPasteHandler={setTelosPrivateKey}
            containerStyle={styles.inputContainer}
          />
          <KInput
            label={'FIO private key'}
            placeholder={'Enter your FIO private key'}
            secureTextEntry
            value={fioPrivateKey}
            onChangeText={setFioPrivateKey}
            onPasteHandler={setFioPrivateKey}
            containerStyle={styles.inputContainer}
          />
          <View style={styles.spacer} />
          <KButton
            title={'Connect account'}
            theme={'blue'}
            style={styles.button}
            renderIcon={() => (
              <Image
                source={require('../../../assets/icons/accounts.png')}
                style={styles.buttonIcon}
              />
            )}
            onPress={_handleConnect}
          />
          <TouchableOpacity style={styles.backButton} onPress={goBack}>
            <MaterialIcon
              name={'keyboard-backspace'}
              size={24}
              color={PRIMARY_BLUE}
            />
          </TouchableOpacity>
        </View>
      </KeyboardAwareScrollView>
    </SafeAreaView>
    );

};

export default connectAccounts()(ConnectAccountScreen);
