import React, { useState } from 'react';
import { View, StyleSheet, TouchableOpacity, Dimensions } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Fio, Ecc } from '@fioprotocol/fiojs';
import CheckBox from 'react-native-check-box';
import { KHeader, KText } from '../../../components';
import { getChain, getEndpoint } from '../../../eos/chains';
import { getBalance } from '../../../eos/tokens';
import { getAccount } from '../../../eos/eos';
import { log } from '../../../logger/logger';
import {
  PRIMARY_GRAY,
  PRIMARY_BLACK,
  PRIMARY_BLUE,
} from '../../../theme/colors';


const fioEndpoint = getEndpoint('FIO');

const { height, width } = Dimensions.get('window');
var addressWidth = width - 60;
var buttonWidth = 40;


const AccountListItem = ({ fioAccount, telosAccount, token, onPress, ...props }) => {
  const [tokenBalance, setTokenBalance] = useState(0);
  const [count, setCount] = useState(0);

  const loadTokenBalance = async (account, token, setTokenBalance) => {
    if (account) {
      getBalance(account.accountName, token, setTokenBalance);
    }
  };

  const handleTokenBalance = (jsonArray) => {
    if(jsonArray && jsonArray.length > 0) {
      setTokenBalance(jsonArray[0]);
    } else if(token) {
      setTokenBalance('0 '+token.name);
    }
  }

  const refreshBalances = () => {
    loadTokenBalance(telosAccount, token, handleTokenBalance);
    setCount(1);
  };

  const handleOnPress = () => {
    setCount(0);
    if (telosAccount) {
      refreshBalances();
    }
    onPress();
  }

  if(count === 0) {
    refreshBalances();
  }

if(telosAccount) {
  return (
      <View onFocus={refreshBalances} style={styles.rowContainer}>
        <View style={[styles.container, props.style]}>
        <TouchableOpacity onPress={handleOnPress}>
          <KText style={styles.address}>{token.name} : {tokenBalance}</KText>
        </TouchableOpacity>
        <TouchableOpacity onPress={refreshBalances}>
          <Icon name={'refresh'} size={25} color="#000000" />
        </TouchableOpacity>
        </View>
      </View>
  );
} else if(fioAccount) {
  return (
      <View style={styles.rowContainer}>
        <View style={[styles.container, props.style]}>
        <TouchableOpacity onPress={handleOnPress}>
          <KText style={styles.address}>No Telos account!</KText>
        </TouchableOpacity>
        </View>
      </View>
  );
} else {
  return (<View style={styles.spacer} />);
}

};

const styles = StyleSheet.create({
  rowContainer: {
    marginTop: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    shadowOffset: { width: 0, height: 2 },
    shadowColor: '#2A2240',
    shadowOpacity: 0.25,
    shadowRadius: 2,
    borderRadius: 6,
    elevation: 4,
    backgroundColor: '#F1F6FF',
    padding: 5,
  },
  address: {
    width: addressWidth,
    marginTop: Platform.OS === 'ios' ? 12 : 0,
    fontSize: 14,
    fontFamily: 'Nunito-Bold',
    color: '#273D52',
    lineHeight: 22,
    marginBottom: 6,
    marginLeft: 6,
  },
});

export default AccountListItem;
