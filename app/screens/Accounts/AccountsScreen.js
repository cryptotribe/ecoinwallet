import React, { useState } from 'react';
import DeviceInfo from 'react-native-device-info';
import { Image,
  View,
  FlatList,
  SafeAreaView,
  Linking,
  Text,
  Alert } from 'react-native';
import { KHeader, KButton } from '../../components';
import styles from './AccountsScreen.style';
import { connectAccounts } from '../../redux';
import { Fio, Ecc } from '@fioprotocol/fiojs';
import ecc from 'eosjs-ecc-rn';
import AccountListItem from './components/AccountListItem';
import { getAccount } from '../../eos/eos';
import { getChain, getEndpoint } from '../../eos/chains';
import { getTokens } from '../../eos/tokens';
import { findIndex } from 'lodash';
import { log } from '../../logger/logger'


const AccountsScreen = props => {
  const [telosLinked, setTelosLinked] = useState(false);

  const {
    addKey,
    connectAccount,
    deleteAccount,
    navigation: { navigate },
    accountsState: { accounts, activeAccountIndex, addresses, keys },
  } = props;

  const tokens = getTokens();
  const fioEndpoint = getEndpoint('FIO');

  var fioAccount;
  var telosAccount;

  var initialConnectedAccounts = accounts;
  const [connectedAccounts, setConnectedAccounts] = useState(initialConnectedAccounts);
  const [runCount, setRunCount] = useState(0);

  accounts.map((value, index, array) => {
    var account = value;
    if (account.chainName === 'TLOS' || account.chainName === 'Telos' ) {
      telosAccount = account;
    }
    if (account.chainName === 'FIO') {
      fioAccount = account;
    }
  });

  const updateAccountLists = (account) => {
      var newConnectedAccounts = [...initialConnectedAccounts , account ];
      initialConnectedAccounts.push(account);
      setConnectedAccounts(newConnectedAccounts);
  };

  const _handleRegisterAddressByEcoin = () => {
    navigate('RegisterAddressEcoin');
  };

  const _handleRegisterAddressByEmail = () => {
    navigate('RegisterAddressEmail');
  };

  const _handlePressToken = (token) => {
    navigate('TokenDetails', { fioAccount, telosAccount, token });
  };

  const getAppVersion = () => {
    return "Version " + DeviceInfo.getVersion() + ", Build " + DeviceInfo.getBuildNumber();
  };

  var optionalButtons = <View style={styles.spacer} />;
  if(!fioAccount) {
    optionalButtons = <View>
        <KButton title={'Register with ECOIN secret code'} theme={'brown'} style={styles.button} icon={'add'} onPress={_handleRegisterAddressByEcoin}/>
        <KButton title={'Register with email verification'} theme={'brown'} style={styles.button} icon={'add'} onPress={_handleRegisterAddressByEmail}/>
      </View>;
  }

  if(accounts.length == 0) {
    return (
     <SafeAreaView style={styles.container}>
      <Image
        style={styles.logo}
        source={require('../../../assets/logo/ecoin-logo.png')}
        resizeMode="contain"
      />
      {optionalButtons}
      <KButton
          title={'Import accounts'}
          theme={'blue'}
          style={styles.button}
          onPress={() => navigate('ConnectAccount')}
          renderIcon={() => (
            <Image
              source={require('../../../assets/icons/accounts.png')}
              style={styles.buttonIcon}
              />
            )}
            />
      <Text style={styles.version}>{getAppVersion()}</Text>
     </SafeAreaView>
    );
  } else {
    return (
     <SafeAreaView style={styles.container}>
      <Image
        style={styles.logo}
        source={require('../../../assets/logo/ecoin-logo.png')}
        resizeMode="contain"
      />
      <KHeader title={fioAccount.address} subTitle={telosAccount.accountName} style={styles.address} />
      <FlatList
        data={tokens}
        keyExtractor={(item, index) => `${index}`}
        renderItem={({ item, index }) => (
          <AccountListItem
            fioAccount={fioAccount}
            telosAccount={telosAccount}
            token={item}
            style={styles.listItem}
            onPress={() => _handlePressToken(item)}
          />
        )}
      />
      {optionalButtons}
      <KButton
        title={'Backup All Keys!'}
        theme={'brown'}
        style={styles.button}
        onPress={() => navigate('BackupAllKeys')}
        renderIcon={() => (
          <Image
            source={require('../../../assets/icons/accounts.png')}
            style={styles.buttonIcon}
          />
        )}
      />
      <Text style={styles.version}>{getAppVersion()}</Text>
     </SafeAreaView>
    );
  }

};

export default connectAccounts()(AccountsScreen);
