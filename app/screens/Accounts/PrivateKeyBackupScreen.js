import React from 'react';
import QRCode from 'react-native-qrcode-svg';
import { Text, Image, SafeAreaView, View, TouchableOpacity, Clipboard, Alert } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { KInput, KHeader, KButton } from '../../components';
import styles from './AccountsScreen.style';
import { connectAccounts } from '../../redux';
import { PRIMARY_BLUE } from '../../theme/colors';


const PrivateKeyBackupScreen = props => {

	const {
    navigation: { navigate, goBack },
		route: {
      params: {
        fioAccount: fioAccount,
        telosAccount: telosAccount,
      },
    },
  } = props;

const getPrivateKeys = () => {
	if(telosAccount && fioAccount) {
		return telosAccount.accountName + ':' + telosAccount.privateKey + ', ' + fioAccount.address + ':' + fioAccount.privateKey;
	} else if(fioAccount) {
		return fioAccount.address + ':' + fioAccount.privateKey;
	} else if(telosAccount) {
		return telosAccount.accountName + ', ' + telosAccount.privateKey;
	} else {
		return 'No private keys!';
	}
};

const copyToClipboard = () => {
	Clipboard.setString(getPrivateKeys());
	Alert.alert('Private keys copied to Clipboard');
};

const getSubtitle = () => {
	if (telosAccount && telosAccount.token && telosAccount.token.name) {
		return telosAccount.token.name + ' on ' + telosAccount.chainName + ': ' + telosAccount.accountName;
	} else if(telosAccount) {
		return telosAccount.chainName + ': ' + telosAccount.accountName;
	} else {
		return 'No Telos account';
	}
};

return (
    <SafeAreaView style={styles.container}>
      <KeyboardAwareScrollView
        contentContainerStyle={styles.scrollContentContainer}
        enableOnAndroid>
        <View style={styles.inner}>
          <TouchableOpacity style={styles.backButton} onPress={goBack}>
            <MaterialIcon
              name={'keyboard-backspace'}
              size={24}
              color={PRIMARY_BLUE}
            />
          </TouchableOpacity>
          	<KHeader title={fioAccount.address} subTitle={getSubtitle()} style={styles.header} />
						<Text style={styles.link} onPress={copyToClipboard}>{getPrivateKeys()}</Text>
						<View style={styles.spacer} />
            <View style={styles.qrcode}>
            	<QRCode value={getPrivateKeys()} size={200}/>
            </View>
						<KButton
            	title={'Copy to Clipboard'}
            	theme={'brown'}
            	style={styles.button}
            	onPress={copyToClipboard}
          	/>
        </View>
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};

export default connectAccounts()(PrivateKeyBackupScreen);
