import React, { useState, useEffect } from 'react';
import { SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  Image,
  FlatList,
  Linking,
  Alert } from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { PieChart, ProgressChart } from 'react-native-chart-kit';
import { KHeader, KButton, KText, KInput } from '../../components';
import styles from './TokenDetailsScreen.style';
import { connectAccounts } from '../../redux';
import ecc from 'eosjs-ecc-rn';
import { Fio, Ecc } from '@fioprotocol/fiojs';
import { getActions } from '../../eos/eos';
import { fioAddPublicAddress } from '../../eos/fio';
import { getChain, getEndpoint } from '../../eos/chains';
import { getBalance, transferToken } from '../../eos/tokens';
import { PRIMARY_BLUE } from '../../theme/colors';
import { findIndex } from 'lodash';
import { log } from '../../logger/logger'

const chars = '12345abcdefghijklmnopqrstuvwxyz';

function randomName() {
    var result = '';
    for (var i = 12; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
    return result;
}

const TokenDetailsScreen = props => {
  const [tokenBalance, setTokenBalance] = useState(0);
  const [fioFee, setFioFee] = useState(0);

  const {
    addKey,
    connectAccount,
    navigation: { navigate, goBack },
    route: {
      params: {
        fioAccount: fioAccount,
        telosAccount: telosAccount,
        token: token,
      },
    },
  } = props;

  const chainCode = 'TLOS';
  const chain = getChain(chainCode);
  const fioEndpoint = getEndpoint('FIO');
  const privateKey = fioAccount.privateKey;
  const fioKey = Ecc.privateToPublic(privateKey);

  const newAccountEndpoint = '';

  const [transactions, setTransactions] = useState([]);
  const [loaded, setLoaded] = useState(false);


  const handleTokenBalance = (jsonArray) => {
    if(jsonArray && jsonArray.length > 0) {
      setTokenBalance(jsonArray[0]);
    } else if(token) {
      setTokenBalance('0 '+token.name);
    }
  };

  const getSubtitle = () => {
    if (telosAccount && token && token.name) {
      return token.name + ' on ' + telosAccount.chainName + ': ' + telosAccount.accountName;
    } else if(telosAccount) {
      return telosAccount.chainName + ': ' + telosAccount.accountName;
    } else {
      return 'No Telos account';
    }
  };

  const _loadBloksHistory = () => {
    if (telosAccount) {
      Linking.openURL('https://telos.bloks.io/account/' + telosAccount.accountName);
    } else {
      Alert.alert('No Telos account!');
    }
  };


  const checkTelosLinking = () => {
    console.log('checkTelosLinking disabled!');
  };


const addTelosAccount = (json, account) => {
  if (json && json.transaction_id) {
    connectAccount(account);
    // Connect Telos account to a FIO address:
    fioAddPublicAddress(fioAccount, account, fioFee);
    telosAccount = account;
    navigate('TokenDetails', { fioAccount, telosAccount });
  } else {
    log({
      description: 'Failed to add account: ' + account.accountName,
      cause: json,
      location: 'TokenDetailsScreen'
    });
  }
};

const createTelosAccount = () => {
  Alert.alert('Create Telos account not supported in UI project');
};

const goToTransfer = () => {
  navigate('Transfer', { token });
};

if(!loaded) {
  if (telosAccount) {
    getBalance(telosAccount.accountName, token, handleTokenBalance);
    checkTelosLinking();
  }
  setLoaded(true);
}

if (telosAccount) {
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.inner}>
        <TouchableOpacity style={styles.backButton} onPress={goBack}>
          <MaterialIcon
            name={'keyboard-backspace'}
            size={24}
            color={PRIMARY_BLUE}
          />
        </TouchableOpacity>
        <KHeader title={fioAccount.address} subTitle={getSubtitle()} style={styles.header} />
        <KText>Balance: {tokenBalance}</KText>
        <FlatList/>
        <KButton
          title={'Load account history'}
          theme={'brown'}
          style={styles.button}
          onPress={_loadBloksHistory}
        />
        <KButton
          title={'Start transfer'}
          theme={'blue'}
          style={styles.button}
          onPress={goToTransfer}
          renderIcon={() => (
            <Image
              source={require('../../../assets/icons/transfer.png')}
              style={styles.buttonIcon}
            />
          )}
        />
      </View>
    </SafeAreaView>
  );
} else {
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.inner}>
        <TouchableOpacity style={styles.backButton} onPress={goBack}>
          <MaterialIcon
            name={'keyboard-backspace'}
            size={24}
            color={PRIMARY_BLUE}
          />
        </TouchableOpacity>
        <KHeader title={fioAccount.address} subTitle={getSubtitle()} style={styles.header} />
        <FlatList/>
        <KButton
          title={'Create Telos account'}
          theme={'brown'}
          style={styles.button}
          onPress={createTelosAccount}
        />
      </View>
    </SafeAreaView>
  );
}


};

export default connectAccounts()(TokenDetailsScreen);
