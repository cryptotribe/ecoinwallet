import { JsonRpc, Api } from 'eosjs-rn';
import { JsSignatureProvider } from 'eosjs-rn/dist/eosjs-jssig';
import { TextEncoder, TextDecoder } from 'text-encoding';
import { getChain, getEndpoint } from './chains';


const getAccount = (accountName, chain) => {
  const endpoint = getEndpoint(chain.name);
  const rpc = new JsonRpc(endpoint);
  return rpc.get_account(accountName);
};

const getActions = (accountName, chain) => {
  const endpoint = getEndpoint(chain.name);
  const rpc = new JsonRpc(endpoint);
  return rpc.history_get_actions(accountName);
};

const getProducers = chain => {
  const endpoint = getEndpoint(chain.name);
  const rpc = new JsonRpc(endpoint);
  return rpc.get_producers(true, '', 100);
};

const sumAmount = (amount1, amount2) => {
  const m1 = parseFloat(amount1.split(' ')[0]);
  const m2 = parseFloat(amount2.split(' ')[0]);
  const totalM = m1 + m2;
  return totalM.toFixed(4);
};

export {
  getAccount,
  getProducers,
  getActions,
  sumAmount,
};
