import { Fio, Ecc } from '@fioprotocol/fiojs';
import ecc from 'eosjs-ecc-rn';
import { Api } from '@fioprotocol/fiojs/dist/chain-api';
import { JsonRpc } from '@fioprotocol/fiojs/dist/tests/chain-jsonrpc';
import { TextEncoder, TextDecoder } from 'text-encoding';
import { getEndpoint } from './chains';
import { log } from '../logger/logger';
import { Alert } from 'react-native';

const expirationPeriod = 100000; // 100 secs from now

// Used to add EOSIO accounts to FIO Address
const fioAddPublicAddress = async (fioAccount, account, fee) => {
  const fioEndpoint = getEndpoint('FIO');
  const rpc = new JsonRpc(fioEndpoint);

  const info = await rpc.get_info();
  const blockInfo = await rpc.get_block(info.last_irreversible_block_num);
  const currentDate = new Date();
  const timePlusTen = currentDate.getTime() + expirationPeriod;
  const timeInISOString = (new Date(timePlusTen)).toISOString();
  const expiration = timeInISOString.substr(0, timeInISOString.length - 1);

  var chainName = account.chainName;
  if(chainName == "Telos") {
    chainName = "TLOS";
  }

  // account is EOSIO (Not FIO account):
  const accPubkey = ecc.privateToPublic(account.privateKey);
  const accName = account.accountName;
  const accIdentifier = accName + ',' + accPubkey;

  const fioAddress = fioAccount.address;
  const fioPublicKey = Ecc.privateToPublic(fioAccount.privateKey);
  const fioActor = Fio.accountHash(fioPublicKey);

  const transaction = {
    expiration,
    ref_block_num: blockInfo.block_num & 0xffff,
    ref_block_prefix: blockInfo.ref_block_prefix,
    actions: [{
      account: 'fio.address',
      name: 'addaddress',
      authorization: [{
        actor: fioActor,
        permission: 'active',
      }],
      data: {
        fio_address: fioAddress,
        public_addresses: [{
          chain_code: chainName,
          token_code: chainName,
          public_address: accIdentifier,
        }],
        max_fee: fee,
        tpid: 'crypto@tribe',
        actor: fioActor,
      },
    }]
  };

  var abiMap = new Map();
  var tokenRawAbi = await rpc.get_raw_abi('fio.address');
  abiMap.set('fio.address', tokenRawAbi);

  const chainId = info.chain_id;
  var privateKeys = [fioAccount.privateKey];

  try {
    const tx = await Fio.prepareTransaction({
      transaction,
      chainId,
      privateKeys,
      abiMap,
      textDecoder: new TextDecoder(),
      textEncoder: new TextEncoder()
    });

    var sendtime = (new Date()).getTime();
    var pushResult = await fetch(fioEndpoint + '/v1/chain/push_transaction', { body: JSON.stringify(tx), method: 'POST', });
    const json = await pushResult.json();
    var calltime = (new Date()).getTime() - sendtime;

    if (!json.processed) {
      log({
        description: 'FIO Add Public Key',
        address: fioAddress,
        transaction: transaction,
        endpoint: fioEndpoint,
        calltime: calltime,
        result: json,
        location: 'fio'
      });
    }
    return json;
  } catch(err) {
    log({
      description: 'FIO Add Public Key Error',
      address: fioAddress,
      transaction: transaction,
      cause: err,
      location: 'fio'
    });
  }
};


export {
  fioAddPublicAddress,
};
