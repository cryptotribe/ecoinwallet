// @flow

const accountsState = {
  accounts: [],
  activeAccountIndex: -1,
  addresses: [],
  keys: []
};


export const defaultReducers = {
  accountsState
};
